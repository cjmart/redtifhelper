﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using MVVM;
using RedChannelTiffHelper.Objects;

namespace RedChannelTiffHelper.ViewModels
{
    class RCTifHelperViewModel: ViewModelBase
    {
        public RCTifHelperViewModel()
        { }

        #region Relay Commands
        RelayCommand _OpenTIFFile;
        public ICommand OpenTIFFileICommand
        {
            get
            {
                if (_OpenTIFFile == null)
                {
                    _OpenTIFFile = new RelayCommand(param => this.ReadTIFFile());
                }
                return _OpenTIFFile;
            }
        }

        RelayCommand _AddCompressionEntry;
        public ICommand AddCompressionEntryICommand
        {
            get
            {
                if (_AddCompressionEntry == null)
                {
                    _AddCompressionEntry = new RelayCommand(param => this.AddCompressionEntry());
                }
                return _AddCompressionEntry;
            }
        }

        RelayCommand _WriteNewFile;
        public ICommand WriteNewFileICommand
        {
            get
            {
                if (_WriteNewFile == null)
                {
                    _WriteNewFile = new RelayCommand(param => this.WriteNewFile());
                }
                return _WriteNewFile;
            }
        }
        
        #endregion

        #region NotifyProperties
        string _TifFileName;
        public string TifFileName
        {
            get { return _TifFileName; }
            set { _TifFileName = value; NotifyPropertyChanged("TifFileName"); }
        }
        string _TagEntries;
        public string TagEntries
        {
            get { return _TagEntries; }
            set { _TagEntries = value; NotifyPropertyChanged("TagEntries"); }
        }
        #endregion

        #region Private Members
        bool _BigEndian = false;
        byte[] _buffer;
        List<ImageFileDirectory> _IFDs;
        #endregion

        private void ReadTIFFile()
        {
            string filePath = OpenTifFile();
            if (filePath != null)
                TifFileName = Path.GetFileName(filePath);
            else
                return;
            GetBinaryData(filePath);
            try
            {
                DetermineEndianness();
                if (_BigEndian)
                    GetBigEndianIFD();
                else
                    GetLittleEndianIFD();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error getting Image File Directory contents.\n" + e.ToString(), "Error", 
                    MessageBoxButton.OK);
                _buffer = null;
                TifFileName = "";
                return;
            }
            WriteTagString();
        }
        private void GetBinaryData(string filePath)
        {
            _buffer = File.ReadAllBytes(filePath);
        }
        private void DetermineEndianness()
        {
            byte I = Convert.ToByte(0x49);
            byte M = Convert.ToByte(0x4D);

            if (_buffer[0] == I && _buffer[1] == I)
                _BigEndian = false;
            else if (_buffer[0] == M && _buffer[1] == M)
                _BigEndian = true;
            else
            {
                MessageBox.Show("File does not appear to be of TIF format!", "Error", MessageBoxButton.OK);
                throw new InvalidDataException();
            }
        }
        private void GetLittleEndianIFD()
        {
            if (_buffer[2] != 0x2A || _buffer[3] != 0x00)
            {
                MessageBox.Show("File is missing TIF identifier: 0042!", "Error", MessageBoxButton.OK);
                throw new InvalidDataException();
            }
            byte[] tmpBuffer = new byte[4];
            Array.Copy(_buffer, 4, tmpBuffer, 0, 4);
            int IFDoffset = BitConverter.ToInt32(tmpBuffer, 0);
            _IFDs = new List<ImageFileDirectory>();
            int countIFD = 0;
            bool lastIFD = false;
            while (!lastIFD)
            {
                _IFDs.Add(new ImageFileDirectory(IFDoffset, ref _buffer));
                if (_IFDs[countIFD].NextIFDOffset == 0)
                    lastIFD = true;
                else
                {
                    IFDoffset = _IFDs[countIFD].NextIFDOffset;
                    countIFD++;
                }
            }
        }
        private void GetBigEndianIFD()
        {
            throw new InvalidDataException("Big Endian files not supported at this time.");
        }
        private void WriteTagString()
        {
            StringBuilder sb = new StringBuilder();
            int count = 0;
            foreach (ImageFileDirectory ifd in _IFDs)
            {
                sb.AppendLine("IFD");
                sb.AppendLine(String.Format(" {0,-10}{1, 5:N0}", "Number:", count));
                sb.AppendLine(String.Format(" {0,-10}{1, 5:N0}", "Offset:", ifd.IFDOffset));
                sb.AppendLine(String.Format(" {0,-10}{1, 5:N0}", "Entries:", ifd.EntryCount));
                sb.AppendLine(String.Format(" {0,-10}{1, 5:N0}", "Tags:", ifd.Tags.Count));
                foreach (Entry tag in ifd.Tags)
                {
                    sb.AppendLine(String.Format("  Tag: 0x{0:X4} ValueOffset: {1, 10:N0} {2, -26} ", tag.Tag, tag.ValueOffset, "(" + tag.Name + ")"));
                }
                count++;
            }
            TagEntries = sb.ToString();
        }

        private void AddCompressionEntry()
        {
            if (_IFDs == null)
            {
                MessageBox.Show("I don't have any image file data loaded!", "Warning", MessageBoxButton.OK, MessageBoxImage.Hand);
                return;
            }
            if (_IFDs[0].Tags.Exists(item => item.Tag == 259))
            {
                MessageBox.Show("The Compression entry (0x103) already exists.", "Warning", 
                    MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            ushort tag = 259;
            EntryType type = EntryType.SHORT;
            int count = 1;
            int VO = 1;
            Entry compression = new Entry()
            {
                Tag = tag,
                Name = "Compression",
                Type = type,
                Count = count,
                ValueOffset = VO
            };

            _IFDs[0].EntryCount++;
            _IFDs[0].Tags.Insert(4, compression);
            for (int i = 0; i < _IFDs[0].Tags.Count; i++)
            {
                if (_IFDs[0].Tags[i].Tag == 273)
                {
                    Entry tmpEntry = _IFDs[0].Tags[i];
                    tmpEntry.ValueOffset += 12;
                    _IFDs[0].Tags[i] = tmpEntry;
                }
                else if (_IFDs[0].Tags[i].Tag == 282)
                {
                    Entry tmpEntry = _IFDs[0].Tags[i];
                    tmpEntry.ValueOffset += 12;
                    _IFDs[0].Tags[i] = tmpEntry;
                }
                else if (_IFDs[0].Tags[i].Tag == 283)
                {
                    Entry tmpEntry = _IFDs[0].Tags[i];
                    tmpEntry.ValueOffset += 12;
                    _IFDs[0].Tags[i] = tmpEntry;
                }
                else if (_IFDs[0].Tags[i].Tag == 270)
                {
                    Entry tmpEntry = _IFDs[0].Tags[i];
                    tmpEntry.ValueOffset += 12;
                    _IFDs[0].Tags[i] = tmpEntry;
                }
            }
            WriteTagString();
        }

        private void WriteNewFile()
        {
            if (_IFDs == null)
            {
                MessageBox.Show("I don't have any image file data loaded!", "Warning", MessageBoxButton.OK, MessageBoxImage.Hand);
                return;
            }
            string newFilePath;
            SaveFileDialog sdlg = new SaveFileDialog();
            sdlg.DefaultExt = ".tif";
            sdlg.Filter = "TIF Files (.tif; .tiff)|*.tif;*.tiff|All files (.*)|*.*";
            sdlg.FileName = Path.GetFileNameWithoutExtension(_TifFileName) + "_EDIT";
            sdlg.InitialDirectory = Path.GetDirectoryName(_TifFileName);
            Nullable<bool> sDlgResult = sdlg.ShowDialog();
            if (sDlgResult == true)
            {
                newFilePath = sdlg.FileName;
            }
            else { sdlg = null; sDlgResult = null; return; }
            FileStream fs = File.Create(newFilePath);
            fs.Write(new byte[] { 0x49, 0x49, 0x2A, 0x00, 0x08, 0x00, 0x00, 0x00 }, 0, 8);
            
            foreach (ImageFileDirectory ifd in _IFDs)
            {
                fs.Write(BitConverter.GetBytes(ifd.EntryCount), 0, 2);
                foreach (Entry tag in ifd.Tags)
                {
                    fs.Write(BitConverter.GetBytes(tag.Tag), 0, 2);
                    
                    fs.Write(BitConverter.GetBytes((ushort)tag.Type), 0, 2);

                    fs.Write(BitConverter.GetBytes(tag.Count), 0, 4);
                    
                    fs.Write(BitConverter.GetBytes(tag.ValueOffset), 0, 4);
                }

                fs.Write(BitConverter.GetBytes(ifd.NextIFDOffset), 0, 4);

                foreach (TagValue value in ifd.Values)
                {
                    fs.Write(value.Data, 0, value.Length);
                }
                foreach (Strip imageStrip in ifd.Strips)
                {
                    fs.Write(imageStrip.Data, 0, imageStrip.Length);
                }
            }
            fs.Close();
        }

        private string OpenTifFile()
        {
            string filePath = null;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "TIF files (*.tif;*.tiff)|*.tif;*.tiff|All files (*.*)|*.*";
            dlg.Multiselect = false;
            dlg.Title = "Open a RED channel TIF image.";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                filePath = dlg.FileName;
                dlg = null; result = null;
            }
            else { dlg = null; result = null; }
            return filePath;
        }
    }
}
