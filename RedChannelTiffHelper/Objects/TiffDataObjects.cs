﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace RedChannelTiffHelper.Objects
{
    class ImageFileDirectory
    {
        public int IFDOffset;
        public ushort EntryCount;
        public List<Entry> Tags;
        public List<TagValue> Values;
        public List<Strip> Strips;
        public int NextIFDOffset;

        private byte[] _buffer;

        public ImageFileDirectory(int offset, ref byte[] buffer)
        {
            IFDOffset = offset;
            _buffer = buffer;
            GetTags();
            GetStrip();
        }

        private void GetTags()
        {
            GetEntryCount();
            if (EntryCount > 0)
            {
                Tags = new List<Entry>();
                Values = new List<TagValue>();
            }
            else
            {
                MessageBox.Show("Entry count is 0 for the Image File Directory!", "Error", MessageBoxButton.OK);
                return;
            }
            for (int i = 0; i < EntryCount; i++)
            {
                ushort tag = GetTag(i);
                EntryType type = GetType(i);
                int count = GetCount(i);
                int VO = GetValueOffset(i, type);

                byte[] tmpValue = new byte[1];
                if (type == EntryType.RATIONAL)
                {
                    tmpValue = new byte[count * 8];
                    tmpValue = ReadEntryValue(VO, count * 8);
                    Values.Add(new TagValue()
                    {
                        Length = tmpValue.Length,
                        Data = tmpValue
                    });
                }
                else if (type == EntryType.ASCII && count > 4)
                {
                    tmpValue = new byte[count];
                    tmpValue = ReadEntryValue(VO, count);
                    Values.Add(new TagValue()
                    {
                        Length = tmpValue.Length,
                        Data = tmpValue
                    });
                }

                Tags.Add(new Entry()
                {
                    Tag = tag,
                    Name = GetTagName(tag),
                    Type = type,
                    Count = count,
                    ValueOffset = VO,
                });
            }
            GetNextIFDOffset();
        }
        private void GetNextIFDOffset()
        {
            NextIFDOffset = BitConverter.ToInt32(_buffer, IFDOffset + 2 + EntryCount * 12);
        }
        private void GetEntryCount()
        {
            EntryCount = BitConverter.ToUInt16(_buffer, IFDOffset);
        }
        private int GetValueOffset(int i, EntryType type)
        {
            return BitConverter.ToInt32(_buffer, IFDOffset + 2 + i * 12 + 8);
        }
        private int GetCount(int i)
        {
            return BitConverter.ToInt32(_buffer, IFDOffset + 2 + i * 12 + 4);
        }
        private EntryType GetType(int i)
        {
            return (EntryType)BitConverter.ToUInt16(_buffer, IFDOffset + 2 + i * 12 + 2);
        }
        private ushort GetTag(int i)
        {
            return BitConverter.ToUInt16(_buffer, IFDOffset + 2 + i * 12);
        }

        private byte[] ReadEntryValue(int offset, int count)
        {
            byte[] bytes = new byte[count];
            Array.Copy(_buffer, offset, bytes, 0, count);
            return bytes;
        }

        private void GetStrip()
        {
            Strips = new List<Strip>();
            int offset = 0;
            int length = 0;

            if (Tags == null)
                return;

            foreach (Entry entry in Tags)
            {
                if (entry.Tag == 273)
                    offset = entry.ValueOffset;
                else if (entry.Tag == 279)
                    length = entry.ValueOffset;
            }
            byte[] data = new byte[length];
            Array.Copy(_buffer, offset, data, 0, length);

            Strips.Add(new Strip()
            {
                Data = data,
                Length = length
            });
        }

        private string GetTagName(ushort tagNum)
        {
            string name = "";

            // These seem to be the most common tags in use for the tiff files scanned from the Epson V700
            switch (tagNum)
            {
                case 0x0FE:
                    name = "SubfileType";
                    break;
                case 0x100:
                    name = "ImageWidth";
                    break;
                case 0x101:
                    name = "ImageLength";
                    break;
                case 0x102:
                    name = "BitsPerSample";
                    break;
                case 0x103:
                    name = "Compression";
                    break;
                case 0x106:
                    name = "PhotometricInterpretation";
                    break;
                case 0x10E:
                    name = "ImageDescription";
                    break;
                case 0x111:
                    name = "StripOffsets";
                    break;
                case 0x115:
                    name = "SamplesPerPixel";
                    break;
                case 0x116:
                    name = "RowsPerStrip";
                    break;
                case 0x117:
                    name = "StripByteCounts";
                    break;
                case 0x11A:
                    name = "XResolution";
                    break;
                case 0x11B:
                    name = "YResolution";
                    break;
                case 0x128:
                    name = "ResolutionUnit";
                    break;
                default:
                    name = "Unregistered Tag";
                    break;
            }
            return name;
        }
    }

    struct Entry
    {
        public ushort Tag;
        public EntryType Type;
        public int Count;
        public int ValueOffset;
        public string Name;
    }

    struct TagValue
    {
        public int Length;
        public byte[] Data;
    }

    struct Strip
    {
        public int Length;
        public byte[] Data;
    }

    enum EntryType: ushort
    {
        BYTE = 1,
        ASCII = 2,
        SHORT = 3,
        LONG = 4,
        RATIONAL = 5,
        SBYTE = 6,
        UNDEFINED = 7,
        SSHORT = 8,
        SLONG = 9,
        SRATIONAL = 10,
        FLOAT = 11,
        DOUBLE = 12
    }

   
}
